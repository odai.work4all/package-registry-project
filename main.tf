resource "hcloud_server" "this" {
  name        = var.server_name
  server_type = var.server_type
  image       = var.image_name
  location    = local.location
  ssh_keys    = local.ssh_keys
  user_data   = local.user_data
  backups     = var.backups
  datacenter  = local.datacenter
  network {
    network_id = var.network_id
  }
  public_net {
    ipv4_enabled = var.public_ips
    ipv6_enabled = var.public_ips
  }
  labels = local.labels
}

resource "hcloud_volume" "this" {
  count             = var.create_volume ? 1 : 0
  name              = "${var.server_name}-volume"
  size              = var.volume_size
  server_id         = hcloud_server.this.id
  automount         = var.automount
  format            = var.volume_format
  delete_protection = var.delete_protection
  labels            = local.labels
}
